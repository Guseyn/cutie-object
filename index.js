module.exports = {

  AreObjectsEqual: require('./src/AreObjectsEqual'),
  AssignedObject: require('./src/AssignedObject'),
  CreatedObject: require('./src/CreatedObject'),
  FrozenObject: require('./src/FrozenObject'),
  HasOwnProperty: require('./src/HasOwnProperty'),
  IsExtensible: require('./src/IsExtensible'),
  IsFrozen: require('./src/IsFrozen'),
  IsPropertyEnumerable: require('./src/IsPropertyEnumerable'),
  IsPrototypeOf: require('./src/IsPrototypeOf'),
  IsSealed: require('./src/IsSealed'),
  Keys: require('./src/Keys'),
  LocaleStringFrom: require('./src/LocaleStringFrom'),
  NotExtensibleObject: require('./src/NotExtensibleObject'),
  ObjectWithDefinedProperties: require('./src/ObjectWithDefinedProperties'),
  ObjectWithDefinedProperty: require('./src/ObjectWithDefinedProperty'),
  ObjectWithPrototypeOf: require('./src/ObjectWithPrototypeOf'),
  OwnPropertyDescriptor: require('./src/OwnPropertyDescriptor'),
  OwnPropertyNames: require('./src/OwnPropertyNames'),
  OwnPropertySymbols: require('./src/OwnPropertySymbols'),
  PrototypeOf: require('./src/PrototypeOf'),
  SealedObject: require('./src/SealedObject'),
  StringFrom: require('./src/StringFrom'),
  Value: require('./src/Value'),
  ValueOf: require('./src/ValueOf')

}
